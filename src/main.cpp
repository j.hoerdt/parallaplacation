
#include <boost/mpi/cartesian_communicator.hpp>
#include <boost/mpi/collectives.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/operations.hpp>

#include <mpi.h>

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

namespace mpi = boost::mpi;

namespace {
using scalar_t = float;
constexpr auto mpi_type_scalar = MPI_FLOAT;

const auto pi = 3.14159265358979323846;

auto optimal_relaxation(std::ptrdiff_t width, std::ptrdiff_t height) {
	return 1.7;
	// return 1.7 - 1.7 * pi / std::min(width, height);
	//  vlt wäre das optimal, keine literatur gesehen: grid_width=sqrt(w*h)
}

struct Coordinate {
	std::ptrdiff_t x;
	std::ptrdiff_t y;
};

struct Data {
	std::ptrdiff_t x_offset;
	std::ptrdiff_t width;
	std::ptrdiff_t height;
	std::ptrdiff_t global_width;
	std::vector<scalar_t> data;

	auto& idx(std::ptrdiff_t x, std::ptrdiff_t y) {
		assert(x >= -1 && x <= width && y >= 0 && y < height);
		return data[y + height * (x + 1)];
	}

	auto& idx(std::ptrdiff_t x, std::ptrdiff_t y) const {
		assert(x >= -1 && x <= width && y >= 0 && y < height);
		return data[y + height * (x + 1)];
	}

	auto to_global(std::ptrdiff_t x, std::ptrdiff_t y) const {
		return Coordinate{.x = x + x_offset, .y = y};
	}
};

auto get_input(const mpi::communicator& world, std::ptrdiff_t size) {
	const auto heat = 100;

	Data input;
	input.global_width = size;
	const auto lower_local_width = input.global_width / world.size();
	const auto tasks_with_lower_width_count =
		world.size() - input.global_width % world.size();

	input.width = world.rank() < tasks_with_lower_width_count
	                  ? lower_local_width
	                  : lower_local_width + 1;
	input.height = size;
	const auto offset_from_lower_width_tasks =
		std::min(std::ptrdiff_t{world.rank()}, tasks_with_lower_width_count) *
		lower_local_width;
	const auto offset_from_higher_width_tasks =
		std::max(world.rank() - tasks_with_lower_width_count, 0l) *
		(lower_local_width + 1);
	input.x_offset =
		offset_from_lower_width_tasks + offset_from_higher_width_tasks;

	input.data.resize(
		(input.width + 2) * input.height,
		std::numeric_limits<scalar_t>::quiet_NaN()
	);

	const auto relative_box_width = 0.025;

	for (auto x = std::ptrdiff_t{0}; x < input.width; ++x) {
		for (auto y = std::ptrdiff_t{0}; y < input.height; ++y) {
			const auto global = input.to_global(x, y);
			if (false) {
			} else if ((global.x > (0.25-relative_box_width)*input.global_width and global.x < (0.25+relative_box_width)*input.global_width and global.y > (0.5-relative_box_width)*input.global_width and global.y < (0.5+relative_box_width)*input.global_width) or (global.x > (0.75-relative_box_width)*input.global_width and global.x < (0.75+relative_box_width)*input.global_width and global.y > (0.5-relative_box_width)*input.global_width and global.y < (0.5+relative_box_width)*input.global_width)) {
				input.idx(x, y) = 0;
			} else if (global.y == 0) {
				input.idx(x, y) = heat;
			} else if (global.x == input.global_width - 1) {
				input.idx(x, y) =
					std::cos(global.y * pi / input.global_width / 2) * heat;
			} else if (global.y == input.height - 1) {
				input.idx(x, y) = 0;
			} else if (global.x == 0) {
				input.idx(x, y) =
					std::cos(global.y * pi / input.global_width / 2) * heat;
			}
		}
	}
	return input;
}

auto get_variable_coordinates(const Data& data) {
	std::vector<Coordinate> variable_coordinates;

	auto is_global_border = [&](auto x, auto y) {
		auto [gx, gy] = data.to_global(x, y);
		return gx == 0 || gy == 0 || gx == data.global_width - 1 ||
		       gy == data.height /*same as global*/ - 1;
	};

	for (auto x = std::ptrdiff_t{0}; x < data.width; ++x) {
		for (auto y = std::ptrdiff_t{0}; y < data.height; ++y) {
			if (std::isnan(data.idx(x, y))) {
				if (is_global_border(x, y)) {
					std::ostringstream ss;
					ss << "Border not boundary at coordinates (" << x << ", "
					   << y << ")";
					throw std::runtime_error{ss.str()};
				}

				variable_coordinates.push_back({x, y});
			}
		}
	}

	return variable_coordinates;
}

auto output_image(
	const Data& data, std::ptrdiff_t max_iterations,
	std::ptrdiff_t actual_iterations, const mpi::communicator& world
) {
	// assumes width >= 2;
	auto [min, max] = std::minmax_element(
		data.data.begin() + data.height, data.data.end() - data.height
	);
	const auto global_min =
		mpi::all_reduce(world, *min, mpi::minimum<scalar_t>{});
	const auto global_max =
		mpi::all_reduce(world, *max, mpi::maximum<scalar_t>{});

	std::ostringstream ss;
	ss << "bla_" << world.rank() << ".pgm";
	std::ofstream output{ss.str()};
	output << "P2\n"
		   << data.width << ' ' << data.height << "\n"
		   << static_cast<int>(global_max - global_min) << "\n"
		   << "#" << max_iterations << ' ' << actual_iterations << '\n';

	for (auto y = std::ptrdiff_t{0}; y < data.height; ++y) {
		for (auto x = std::ptrdiff_t{0}; x < data.width; ++x) {
			output << static_cast<int>(data.idx(x, y) - global_min) << ' ';
		}
		output << '\n';
	}
}

auto make_first_guess(Data& data) {
	for (auto& value : data.data) {
		// nans are supposed to be variable / not part of boundary condition
		// need to initialize local boundary as well to avoid nans propagating
		// in first iteration before exchange
		if (std::isnan(value)) {
			value = 1;
		}
	}
}

template <bool with_residual>
auto zeitschritt(
	Data& data, const mpi::communicator& cart, scalar_t precision,
	scalar_t relaxation_factor,
	const std::vector<Coordinate>& variable_coordinates, int left_neighbor,
	int right_neighbor
) {
	scalar_t residual = 0;
	for (auto [x, y] : variable_coordinates) {
		const auto old_value = data.idx(x, y);
		const auto average = (data.idx(x, y - 1) + data.idx(x - 1, y) +
		                      data.idx(x + 1, y) + data.idx(x, y + 1)) /
		                     4;
		data.idx(x, y) =
			data.idx(x, y) + relaxation_factor * (average - data.idx(x, y));

		if constexpr (with_residual) {
			residual += std::pow(data.idx(x, y) - old_value, 2);
		}
	}

	MPI_Request border_exchanges[4];
	if (const auto result = MPI_Isend(
			&data.idx(0, 0), data.height, mpi_type_scalar, left_neighbor, 0,
			cart, &border_exchanges[0]
		);
	    result != MPI_SUCCESS) {
		throw mpi::exception{"MPI_Sendrecv", result};
	}
	if (const auto result = MPI_Isend(
			&data.idx(data.width - 1, 0), data.height, mpi_type_scalar,
			right_neighbor, 0, cart, &border_exchanges[1]
		);
	    result != MPI_SUCCESS) {
		throw mpi::exception{"MPI_Sendrecv", result};
	}
	if (const auto result = MPI_Irecv(
			&data.idx(data.width, 0), data.height, mpi_type_scalar,
			right_neighbor, 0, cart, &border_exchanges[2]
		);
	    result != MPI_SUCCESS) {
		throw mpi::exception{"MPI_Sendrecv", result};
	}
	if (const auto result = MPI_Irecv(
			&data.idx(-1, 0), data.height, mpi_type_scalar, left_neighbor, 0,
			cart, &border_exchanges[3]
		);
	    result != MPI_SUCCESS) {
		throw mpi::exception{"MPI_Sendrecv", result};
	}
	MPI_Waitall(
		std::size(border_exchanges), border_exchanges, MPI_STATUSES_IGNORE
	);

	if constexpr (with_residual) {
		residual = mpi::all_reduce(cart, residual, std::plus<>{});
		return residual;
	}
}

} // namespace

int main(int argc, char** argv) {
	mpi::environment env;

	auto cart = [] {
		mpi::communicator world;
		mpi::cartesian_dimension dims[] = {{world.size(), false}};
		return mpi::cartesian_communicator(
			world, mpi::cartesian_topology(dims), true
		);
	}();

	const auto [left_neighbor, right_neighbor] = cart.shifted_ranks(0, 1);

	std::vector<std::string> args{argv, argv + argc};
	const std::ptrdiff_t grid_width = std::sqrt(std::stoll(args.at(1))) * 1000;
	if (cart.rank() == 0) {
		std::clog << "grid width and height: " << grid_width << '\n';
	}
	auto data = get_input(cart, grid_width);
	const auto precision = 1e-1;

	const auto relaxation_factor = optimal_relaxation(data.width, data.height);
	if (cart.rank() == 0) {
		std::clog << "relaxation factor: " << relaxation_factor << '\n';
	}
	const auto variable_coordinates = get_variable_coordinates(data);

	make_first_guess(data);

	if (cart.rank() == 0) {
		std::clog << "initialization complete\n";
	}

	const auto start_time = std::chrono::steady_clock::now();
	const auto max_iterations = 20000;
	auto i = 0;
	for (; i < max_iterations; ++i) {
		for (auto j = 0; j < 100; ++j, ++i) {
			zeitschritt<false>(
				data, cart, precision, relaxation_factor, variable_coordinates,
				left_neighbor, right_neighbor
			);
		}

		const auto residual = zeitschritt<true>(
			data, cart, precision, relaxation_factor, variable_coordinates,
			left_neighbor, right_neighbor
		);
		// if (cart.rank() == 0) {
		// 	std::clog << "residual: " << residual << "                    " <<
		// '\r';
		// }
		// if (residual < precision) {
		// 	break;
		// }
	}
	// std::clog << '\n';

	const auto end_time = std::chrono::steady_clock::now();
	const auto duration_seconds =
		std::chrono::duration<float>(end_time - start_time).count();
	if (cart.rank() == 0) {
		float global_max_duration_seconds;
		mpi::reduce(
			cart, duration_seconds, global_max_duration_seconds,
			mpi::maximum<float>{}, 0
		);
		std::clog << "calculation took "
				  << std::setprecision(std::numeric_limits<float>::max_digits10)
				  << global_max_duration_seconds << "s\n";
	} else {
		mpi::reduce(cart, duration_seconds, mpi::maximum<float>{}, 0);
	}

	// std::clog << i << '\n';

	output_image(data, max_iterations, i, cart);
}
