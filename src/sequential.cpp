
#include <omp.h>

#include <iostream>
#include <vector>
#include <limits>
#include <cmath>
#include <sstream>
#include <algorithm>

#define bar( a ) ( a * 2 )

using scalar_t = float;

namespace {
const auto pi = 3.14159265358979323846;

auto optimal_relaxation(long grid_width) {
    return 2-2*pi/grid_width;
    // vlt wäre das optimal, keine literatur gesehen: grid_width=sqrt(w*h)
}

struct Data {
    std::ptrdiff_t width;
    std::ptrdiff_t height;
    std::vector<scalar_t> data;

    auto& idx(std::ptrdiff_t x, std::ptrdiff_t y) {
        return data[x + width * y];
    }

    auto& idx(std::ptrdiff_t x, std::ptrdiff_t y) const {
        return data[x + width * y];
    }
};

auto get_input() {
    const auto heat = 100;

    Data input;
    input.width = 1000;
    input.height = 1000;
    input.data.resize(input.width*input.height, std::numeric_limits<scalar_t>::quiet_NaN());


    for (auto y = 0z; y < input.height; ++y) {
        for (auto x = 0z; x < input.width; ++x) {
            if (y == 0) {
                input.idx(x,y) = heat;
            } else if (x == 0 || x == input.width-1 || y == input.height-1) {
                input.idx(x,y) = 0;
            }
        }
    }

    return input;
}

struct Coordinate {
    std::ptrdiff_t x;
    std::ptrdiff_t y;
};

auto get_variable_coordinates(const Data& data) {
    std::vector<Coordinate> variable_coordinates;

    auto is_border = [width=data.width, height=data.height] (auto x, auto y) {
        return x == 0 || y == 0 || x == width -1 || y == height - 1;
    };

    for (auto y = 0z; y < data.height; ++y) {
        for (auto x = 0z; x < data.width; ++x) {
            if (std::isnan(data.idx(x,y))) {
                if (is_border(x,y)) {
                    std::ostringstream ss;
                    ss << "Border not boundary at coordinates (" << x << ", " << y << ")";
                    throw std::runtime_error{ss.str()};
                }

                variable_coordinates.push_back({x,y});
            }

        }
    }

    return variable_coordinates;
}

auto make_first_guess(Data& data, const std::vector<Coordinate>& variable_coordinates) {
    for (auto [x, y] : variable_coordinates) {
        data.idx(x,y) = 0;
    }
}

} // namespace

int main() {
    const auto precision = 0.1;

    auto data = get_input();

    const auto relaxation_factor = optimal_relaxation(data.width);
    const auto variable_coordinates = get_variable_coordinates(data);

    make_first_guess(data, variable_coordinates);

    const auto max_iterations = 10000;
    auto i = 0;
    for (; i < max_iterations; ++i) {
        scalar_t residual = 0;
        for (auto [x,y] : variable_coordinates) {
            const auto old_value = data.idx(x,y);
            const auto average = (data.idx(x,y-1) + data.idx(x-1,y) + data.idx(x+1,y) + data.idx(x,y+1)) / 4;
            data.idx(x,y) = data.idx(x,y) + relaxation_factor * (average - data.idx(x,y));

            residual += std::pow(data.idx(x,y) - old_value, 2);
        }

        std::clog << residual << ' ';
        if (residual < precision) {
            break;
        }
    }

    std::clog << i << '\n';

    const auto max_value = *std::max_element(data.data.begin(), data.data.end());

    std::cout << "P2\n" << data.width << ' ' << data.height << "\n" << max_value << "\n" << "#" << max_iterations << ' ' << i << '\n';

    
    for (auto y = 0z; y < data.height; ++y) {
        for (auto x = 0z; x < data.width; ++x) {
            std::cout << static_cast<int>(data.idx(x,y)) << ' ';
        }
        std::cout << '\n';
    }
}