# parallaplacation

## About the project
This project was developed by Jakob Hördt and Philipp Müller as part of the module "High-performance-computing" at Georg August Universität Göttingen. Supervising was Jonathan Decker.

## Usage
While the src folder contains two files, only the main.cpp file is the real project. The secuential.cpp is an early, unparallelized prototype. 
Before compiling the main.cpp with the command "mpicxx -lboost_mpi -std=c++17 main.cpp -O2 -march=native -o test" for the amp nodes on the scientific compute cluster Göttingen, the user can hardcode his own scenario into the get input function. The program works fine with the default scenario.

When used on a compute cluster with more than one thread, the program outputs as many pgm snippets as threads where used. Those snippets can be put together with the command 
"convert +append 0.pgm 1.pgm 2.pgm 3.pgm out.png" as example for four outputs. 
To assign the program to a node for calculation, we use an sbatch command "sbatch --ntasks-per-node=1 --nodes=1 --time=5:00 --wrap="srun /home/uni08/hpctraining64/test 1" -C scratch" as an example for one task. As the recent version is the version weak scaling was tested with, the 1 entered indicates the number of processes used. In the program, it sets the grid size to $1000*sqrt(parameter)$.
